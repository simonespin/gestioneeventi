package it.epicode.gestioneEventi.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import it.epicode.gestioneEventi.entities.Concerto;
import it.epicode.gestioneEventi.entities.Evento;
import it.epicode.gestioneEventi.entities.GaraDiAtletica;
import it.epicode.gestioneEventi.entities.Genere;
import it.epicode.gestioneEventi.entities.Location;
import it.epicode.gestioneEventi.entities.Partecipazione;
import it.epicode.gestioneEventi.entities.PartitaDiCalcio;
import it.epicode.gestioneEventi.entities.Persona;
import it.epicode.gestioneEventi.entities.SquadraVincente;
import it.epicode.gestioneEventi.entities.Stato;
import it.epicode.gestioneEventi.persistence.IEventoRepository;
import it.epicode.gestioneEventi.persistence.IPartitaRepository;
import it.epicode.gestioneEventi.persistence.IRepository;
import it.epicode.gestioneEventi.persistence.Repository;
import it.epicode.gestioneEventi.utils.JpaUtil;

public class EventoService implements AbstractEventoService {
	private EntityManager em;
	private IEventoRepository eventoRepo;
	private IPartitaRepository partitaReport;
	private IRepository<Persona, Long> personaRepo;
	private IRepository<Location, Long> locationRepo;
	private IRepository<Partecipazione, Long> partecipazioneRepo;
	private IRepository<PartitaDiCalcio, Long> partitaRepo;
	private IRepository<GaraDiAtletica, Long> garaRepo;
	private IRepository<Concerto, Long> concertoRepo;

	public EventoService(EntityManager em, IEventoRepository repo, IPartitaRepository report,
			IRepository<Persona, Long> personaRepo, IRepository<Location, Long> lR,
			IRepository<Partecipazione, Long> pR, IRepository<PartitaDiCalcio, Long> pCR,
			IRepository<GaraDiAtletica, Long> gA, IRepository<Concerto, Long> con) {
		this.personaRepo = personaRepo;
		this.eventoRepo = repo;
		this.em = em;
		this.locationRepo = lR;
		this.partecipazioneRepo = pR;
		this.partitaRepo = pCR;
		this.garaRepo = gA;
		this.concertoRepo = con;
		this.partitaReport = report;

	}

	@Override
	public Evento registraEvento(Evento newEvento) {
		try {
			em.getTransaction().begin();
			eventoRepo.salva(newEvento);
			em.getTransaction().commit();
			return newEvento;

		} catch (PersistenceException ex) {
			em.getTransaction().rollback();
			throw ex;
		}

	}

	@Override
	public Evento getById(Long id) {

		Evento ev = eventoRepo.getById(id);
		return ev;
	}

	@Override
	public void delete(Long id) {

		try {
			em.getTransaction().begin();
			eventoRepo.delete(id);
			em.getTransaction().commit();
		} catch (PersistenceException ex) {
			em.getTransaction().rollback();
			throw ex;
		}

	}

	@Override
	public Partecipazione registraPartecipazione(Long idPersona, Long idEvento) {
		try {
			em.getTransaction().begin();
			Persona pers = personaRepo.getById(idPersona);
			Evento evento = eventoRepo.getById(idEvento);
			Partecipazione part = new Partecipazione();
			part.setEvento(evento);
			part.setPersona(pers);
			part.setStato(Stato.DA_CONFERMARE);
			evento.getPartecipazioni().add(part);
			partecipazioneRepo.salva(part);
			em.getTransaction().commit();
			return part;
		} catch (PersistenceException ex) {
			em.getTransaction().rollback();
			throw ex;
		}
	}

	@Override
	public Persona registraPersona(Persona p) {
		try {
			em.getTransaction().begin();
			personaRepo.salva(p);
			em.getTransaction().commit();
			return p;
		} catch (PersistenceException ex) {
			em.getTransaction().rollback();
			throw ex;
		}
	}

	@Override
	public Location registraLocation(Location l) {
		try {
			em.getTransaction().begin();
			locationRepo.salva(l);
			em.getTransaction().commit();
			return l;
		} catch (PersistenceException ex) {
			em.getTransaction().rollback();
			throw ex;
		}
	}

	@Override
	public PartitaDiCalcio registraPartita(PartitaDiCalcio pDC) {
		try {
			em.getTransaction().begin();
			partitaRepo.salva(pDC);
			em.getTransaction().commit();
			return pDC;
		} catch (PersistenceException ex) {
			em.getTransaction().rollback();
			throw ex;
		}

	}

	@Override
	public GaraDiAtletica registraGara(GaraDiAtletica gA) {
		try {
			em.getTransaction().begin();
			garaRepo.salva(gA);
			em.getTransaction().commit();
			return gA;
		} catch (PersistenceException ex) {
			em.getTransaction().rollback();
			throw ex;
		}
	}

	@Override
	public Concerto registraConcerto(Concerto con) {
		try {
			em.getTransaction().begin();
			concertoRepo.salva(con);
			em.getTransaction().commit();
			return con;
		} catch (PersistenceException ex) {
			em.getTransaction().rollback();
			throw ex;
		}
	}

	@Override
	public List<Concerto> getConcertiStreaming(boolean inStream) {

		return eventoRepo.getConcertiStreaming(inStream);
	}

	@Override
	public List<Concerto> getConcertiDiGenere(List<Genere> generi) {

		return eventoRepo.getConcertiDiGenere(generi);
	}

	@Override
	public List<PartitaDiCalcio> getPartiteVinteInCasa(List<SquadraVincente> risultato) {

		return partitaReport.getPartiteVinteInCasa(risultato);

	}

	@Override
    public List<PartitaDiCalcio> getPartiteVinteInTrasferta(List<SquadraVincente> risultato) {
		
		return partitaReport.getPartiteVinteInTrasferta(risultato);
		
	}
	
	@Override
	public List<PartitaDiCalcio> getPartitePareggiate(List<SquadraVincente> risultato){
		
		return partitaReport.getPartitePareggiate(risultato);
	}
	
}
