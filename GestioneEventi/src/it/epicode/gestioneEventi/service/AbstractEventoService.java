package it.epicode.gestioneEventi.service;

import java.util.List;

import it.epicode.gestioneEventi.entities.Concerto;
import it.epicode.gestioneEventi.entities.Evento;
import it.epicode.gestioneEventi.entities.GaraDiAtletica;
import it.epicode.gestioneEventi.entities.Genere;
import it.epicode.gestioneEventi.entities.Location;
import it.epicode.gestioneEventi.entities.Partecipazione;
import it.epicode.gestioneEventi.entities.PartitaDiCalcio;
import it.epicode.gestioneEventi.entities.Persona;
import it.epicode.gestioneEventi.entities.SquadraVincente;

public interface AbstractEventoService{
	
    Evento registraEvento(Evento newEvento);
    Evento getById(Long id);
	void delete(Long id);
	Partecipazione registraPartecipazione(Long idPersona, Long idEvento);
	Persona registraPersona(Persona p);
	Location registraLocation(Location l);
	PartitaDiCalcio registraPartita(PartitaDiCalcio pDC);
	GaraDiAtletica registraGara(GaraDiAtletica gA);
	Concerto registraConcerto(Concerto con);
	List<Concerto> getConcertiStreaming(boolean inStream);
	List<Concerto> getConcertiDiGenere(List<Genere> generi);
	List<PartitaDiCalcio> getPartiteVinteInCasa(List<SquadraVincente> risultato);
	List<PartitaDiCalcio> getPartiteVinteInTrasferta(List<SquadraVincente> risultato);
	List<PartitaDiCalcio> getPartitePareggiate(List<SquadraVincente> risultato);
}
