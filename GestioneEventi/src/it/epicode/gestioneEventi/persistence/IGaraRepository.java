package it.epicode.gestioneEventi.persistence;

import java.util.List;

import it.epicode.gestioneEventi.entities.Evento;
import it.epicode.gestioneEventi.entities.GaraDiAtletica;

public interface IGaraRepository extends IRepository<Evento, Long>{
	
	List<GaraDiAtletica> getGareDiAtleticaPerVincitore();

}
