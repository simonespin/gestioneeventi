package it.epicode.gestioneEventi.persistence;

import java.util.List;

import it.epicode.gestioneEventi.entities.Concerto;
import it.epicode.gestioneEventi.entities.Evento;
import it.epicode.gestioneEventi.entities.Genere;
import it.epicode.gestioneEventi.entities.PartitaDiCalcio;

public interface IEventoRepository extends IRepository<Evento, Long> {
	
	List<Concerto> getConcertiStreaming(boolean inStream);
	List<Concerto> getConcertiDiGenere(List<Genere> generi);


}
