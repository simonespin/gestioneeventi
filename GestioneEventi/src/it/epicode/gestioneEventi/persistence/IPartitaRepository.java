package it.epicode.gestioneEventi.persistence;

import java.util.List;

import it.epicode.gestioneEventi.entities.Evento;
import it.epicode.gestioneEventi.entities.PartitaDiCalcio;
import it.epicode.gestioneEventi.entities.SquadraVincente;

public interface IPartitaRepository extends IRepository<Evento, Long> {
	
	List<PartitaDiCalcio> getPartiteVinteInCasa(List<SquadraVincente> risultato);
	List<PartitaDiCalcio> getPartiteVinteInTrasferta(List<SquadraVincente> risultato);
	List<PartitaDiCalcio> getPartitePareggiate(List<SquadraVincente> risultato);
}
