package it.epicode.gestioneEventi.persistence;

import javax.persistence.EntityManager;


public class Repository<T> implements IRepository<T, Long>{
	protected EntityManager em;
	protected Class entityClass;

	public Repository(EntityManager em, Class clazz) {
		this.em = em;
		this.entityClass = clazz;
	}

	@Override
	public T salva(T newElement) {

		em.persist(newElement);

		return newElement;
	}

	@Override
	public T getById(Long id) {

		return  (T)em.find(entityClass, id);
		
	}

	@Override
	public void delete(Long id) {

		T ev = (T)em.find(entityClass, id);
		em.remove(ev);

	}

	@Override
	public void refresh(T element) {
		em.refresh(element);

	}
}
