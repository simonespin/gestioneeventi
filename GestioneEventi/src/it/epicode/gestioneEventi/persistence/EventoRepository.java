package it.epicode.gestioneEventi.persistence;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import it.epicode.gestioneEventi.entities.Concerto;
import it.epicode.gestioneEventi.entities.Evento;
import it.epicode.gestioneEventi.entities.Genere;
import it.epicode.gestioneEventi.entities.PartitaDiCalcio;

public class EventoRepository extends Repository<Evento> implements IEventoRepository{

	public EventoRepository(EntityManager em) {
		super(em, Evento.class);

	}

	@Override
	public List<Concerto> getConcertiStreaming(boolean inStream) {
		Query q = em.createNamedQuery("concertiInStreaming");
		q.setParameter("isStreaming", inStream);

		return q.getResultList();

	}

	@Override
	public List<Concerto> getConcertiDiGenere(List<Genere> generi) {
		Query q = em.createNamedQuery("concertiDiGenere");
		q.setParameter("listaGeneri", generi);

		return q.getResultList();
	}



}
