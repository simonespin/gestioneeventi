package it.epicode.gestioneEventi.persistence;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import it.epicode.gestioneEventi.entities.Evento;
import it.epicode.gestioneEventi.entities.PartitaDiCalcio;
import it.epicode.gestioneEventi.entities.SquadraVincente;

public class PartitaRepository extends Repository<Evento> implements IPartitaRepository {

	public PartitaRepository(EntityManager em) {
		super(em, Evento.class);

	}

	@Override
	public List<PartitaDiCalcio> getPartiteVinteInCasa(List<SquadraVincente> risultato) {
		Query q = em.createNamedQuery("vinteInCasa");
		q.setParameter("CASA", risultato);
		
		return q.getResultList();
	}

	@Override
	public List<PartitaDiCalcio> getPartiteVinteInTrasferta(List<SquadraVincente> risultato) {
		Query q = em.createNamedQuery("vinteInTrasferta");
		q.setParameter("TRASFERTA", risultato);
		
		return q.getResultList();
	}

	@Override
	public List<PartitaDiCalcio> getPartitePareggiate(List<SquadraVincente> risultato) {
		Query q = em.createNamedQuery("pareggiate");
		q.setParameter("PAREGGIO", risultato);
		
		return q.getResultList();
	}

}