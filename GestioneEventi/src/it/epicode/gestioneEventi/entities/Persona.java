package it.epicode.gestioneEventi.entities;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="persona")
public class Persona {
	
	@Id
	@SequenceGenerator(name="persona_seq", sequenceName="persona_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="persona_seq")
	private long id;
	private String nome;
	private String cognome;
	private String email;
	@Column(name="data_di_nascita")
	private LocalDate dataDiNascita;
	@Enumerated(EnumType.STRING)
	private Sesso sesso;
	@OneToMany(mappedBy="persona", cascade=CascadeType.REMOVE)
	@OrderBy("evento.dataEvento ASC")
	private List<Partecipazione> listaPartecipazioni;
	
	public Persona() {}

	public Persona(long id, String nome, String cognome, String email, LocalDate dataDiNascita, Sesso sesso,
			List<Partecipazione> listaPartecipazioni) {
		super();
		this.id = id;
		this.nome = nome;
		this.cognome = cognome;
		this.email = email;
		this.dataDiNascita = dataDiNascita;
		this.sesso = sesso;
		this.listaPartecipazioni = listaPartecipazioni;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public LocalDate getDataDiNascita() {
		return dataDiNascita;
	}

	public void setDataDiNascita(LocalDate dataDiNascita) {
		this.dataDiNascita = dataDiNascita;
	}

	public Sesso getSesso() {
		return sesso;
	}

	public void setSesso(Sesso sesso) {
		this.sesso = sesso;
	}

	public List<Partecipazione> getListaPartecipazioni() {
		return listaPartecipazioni;
	}

	public void setListaPartecipazioni(List<Partecipazione> listaPartecipazioni) {
		this.listaPartecipazioni = listaPartecipazioni;
	}
	
	
}
