package it.epicode.gestioneEventi.entities;

public enum Genere {

	CLASSICO, ROCK, POP;
}
