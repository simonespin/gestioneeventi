package it.epicode.gestioneEventi.entities;

import java.time.LocalDate;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;

@Entity
@NamedQuery(name = "garaPerVincitore", query = "select g from GaraDiAtletica g  where g.vincitore in :listaGeneri")
public class GaraDiAtletica extends Evento {

	@ManyToMany
	private Set<Persona> atleti;

	@ManyToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name = "id_persona")
	private Persona vincitore;

	public GaraDiAtletica() {

	}

	public GaraDiAtletica(long id, String titolo, LocalDate dataEvento, String descrizione, TipoEvento tipoEvento,
			int numeroMaxPartecipanti, Location location, Set<Partecipazione> sP, Set<Persona> atleti,
			Persona vincitore) {
		super(id, titolo, dataEvento, descrizione, tipoEvento, numeroMaxPartecipanti, location, sP);
		this.atleti = atleti;
		this.vincitore = vincitore;
	}

	public Set<Persona> getAtleti() {
		return atleti;
	}

	public void setAtleti(Set<Persona> atleti) {
		this.atleti = atleti;
	}

	public Persona getVincitore() {
		return vincitore;
	}

	public void setVincitore(Persona vincitore) {
		this.vincitore = vincitore;
	}

}
