package it.epicode.gestioneEventi.entities;

import java.time.LocalDate;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.NamedQuery;

@Entity
@NamedQuery(name = "concertiInStreaming", query = "select c from Concerto c where c.isStreaming = :isStreaming")
@NamedQuery(name = "concertiDiGenere", query = "select c from Concerto c where c.genere in :listaGeneri")
public class Concerto extends Evento{
	
	@Column(name ="genere")
	@Enumerated(EnumType.STRING)
	private Genere genere;
	
	@Column(name ="is_streaming")
	private boolean isStreaming;
	
	public Concerto() {
		
	}

	public Concerto(long id, String titolo, LocalDate dataEvento, String descrizione, TipoEvento tipoEvento,
			int numeroMaxPartecipanti, Location location, Set<Partecipazione> sP, Genere genere, boolean isStreaming) {
		super(id, titolo, dataEvento, descrizione, tipoEvento, numeroMaxPartecipanti, location, sP);
		this.genere = genere;
		this.isStreaming = isStreaming;
	}

	public Genere getGenere() {
		return genere;
	}

	public void setGenere(Genere genere) {
		this.genere = genere;
	}

	public boolean isStreaming() {
		return isStreaming;
	}

	public void setStreaming(boolean isStreaming) {
		this.isStreaming = isStreaming;
	}

	
	
}
