package it.epicode.gestioneEventi.entities;

import java.time.LocalDate;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.NamedQuery;

@Entity
@NamedQuery(name = "vinteInCasa", query = "select x from PartitaDiCalcio x where x.squadraVincente = :CASA")
@NamedQuery(name = "vinteInTrasferta", query = "select x from PartitaDiCalcio x where x.squadraVincente = :TRASFERTA")
@NamedQuery(name = "pareggiate", query = "select x from PartitaDiCalcio x where x.squadraVincente LIKE :PAREGGIO")
public class PartitaDiCalcio extends Evento {

	@Column(name = "squadra_di_casa")
	private String squadraDiCasa;

	@Column(name = "squadra_ospite")
	private String squadraOspite;

	@Column(name = "squadra_vincente")
	@Enumerated(EnumType.STRING)
	private SquadraVincente squadraVincente;

	@Column(name = "numero_gol_squadra_casa")
	private int numeroGolSquadraDiCasa;

	@Column(name = "numero_gol_squadra_ospite")
	private int numeroGolSquadraOspite;

	public PartitaDiCalcio() {

	}

	public PartitaDiCalcio(long id, String titolo, LocalDate dataEvento, String descrizione, TipoEvento tipoEvento,
			int numeroMaxPartecipanti, Location location, Set<Partecipazione> sP, String squadraDiCasa,
			String squadraOspite, SquadraVincente squadraVincente, int numeroGolSquadraDiCasa, int numeroGolSquadraOspite) {
		super(id, titolo, dataEvento, descrizione, tipoEvento, numeroMaxPartecipanti, location, sP);
		this.squadraDiCasa = squadraDiCasa;
		this.squadraOspite = squadraOspite;
		this.squadraVincente = squadraVincente;
		this.numeroGolSquadraDiCasa = numeroGolSquadraDiCasa;
		this.numeroGolSquadraOspite = numeroGolSquadraOspite;
	}

	public String getSquadraDiCasa() {
		return squadraDiCasa;
	}

	public void setSquadraDiCasa(String squadraDiCasa) {
		this.squadraDiCasa = squadraDiCasa;
	}

	public String getSquadraOspite() {
		return squadraOspite;
	}

	public void setSquadraOspite(String squadraOspite) {
		this.squadraOspite = squadraOspite;
	}

	
	

	public SquadraVincente getSquadraVincente() {
		return squadraVincente;
	}

	public void setSquadraVincente(SquadraVincente squadraVincente) {
		this.squadraVincente = squadraVincente;
	}

	public int getNumeroGolSquadraDiCasa() {
		return numeroGolSquadraDiCasa;
	}

	public void setNumeroGolSquadraDiCasa(int numeroGolSquadraDiCasa) {
		this.numeroGolSquadraDiCasa = numeroGolSquadraDiCasa;
	}

	public int getNumeroGolSquadraOspite() {
		return numeroGolSquadraOspite;
	}

	public void setNumeroGolSquadraOspite(int numeroGolSquadraOspite) {
		this.numeroGolSquadraOspite = numeroGolSquadraOspite;
	}

}
