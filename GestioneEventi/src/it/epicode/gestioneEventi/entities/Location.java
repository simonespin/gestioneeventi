package it.epicode.gestioneEventi.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table (name="location")

public class Location {
	@Id
	@SequenceGenerator(name="location_seq", sequenceName="location_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="location_seq")
	private long id;
	private String nome;
	private String citta;
	
	public Location() {}
	
	public Location(long id, String nome, String citta) {
		this.id = id;
		this.nome = nome;
		this.citta = citta;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCitta() {
		return citta;
	}
	public void setCitta(String citta) {
		this.citta = citta;
	}
	
	
	

}
