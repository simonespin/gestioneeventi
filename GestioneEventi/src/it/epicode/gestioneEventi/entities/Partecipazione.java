package it.epicode.gestioneEventi.entities;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="partecipazione")
public class Partecipazione {
	
	@Id
	@SequenceGenerator(name="partecipazione_seq", sequenceName="partecipazione_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="partecipazione_seq")
	private long id;
	@Enumerated(EnumType.STRING)
	private Stato stato;
	@ManyToOne
	@JoinColumn(name="id_evento")
	private Evento evento;
	@ManyToOne
	@JoinColumn(name="id_persona")
	private Persona persona;
	
	public Partecipazione() {}

	public Partecipazione(long id, Stato stato, Evento evento, Persona persona) {
		super();
		this.id = id;
		this.stato = stato;
		this.evento = evento;
		this.persona = persona;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Stato getStato() {
		return stato;
	}

	public void setStato(Stato stato) {
		this.stato = stato;
	}

	public Evento getEvento() {
		return evento;
	}

	public void setEvento(Evento evento) {
		this.evento = evento;
	}

	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}
	
	
}
