package it.epicode.gestioneEventi.start;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import javax.persistence.EntityManager;

import it.epicode.gestioneEventi.entities.Concerto;
import it.epicode.gestioneEventi.entities.Evento;
import it.epicode.gestioneEventi.entities.GaraDiAtletica;
import it.epicode.gestioneEventi.entities.Genere;
import it.epicode.gestioneEventi.entities.Location;
import it.epicode.gestioneEventi.entities.Partecipazione;
import it.epicode.gestioneEventi.entities.PartitaDiCalcio;
import it.epicode.gestioneEventi.entities.Persona;
import it.epicode.gestioneEventi.entities.Sesso;
import it.epicode.gestioneEventi.entities.SquadraVincente;
import it.epicode.gestioneEventi.entities.TipoEvento;
import it.epicode.gestioneEventi.persistence.EventoRepository;
import it.epicode.gestioneEventi.persistence.IEventoRepository;
import it.epicode.gestioneEventi.persistence.IPartitaRepository;
import it.epicode.gestioneEventi.persistence.IRepository;
import it.epicode.gestioneEventi.persistence.PartitaRepository;
import it.epicode.gestioneEventi.persistence.Repository;
import it.epicode.gestioneEventi.service.AbstractEventoService;
import it.epicode.gestioneEventi.service.EventoService;
import it.epicode.gestioneEventi.utils.JpaUtil;

public class Main {

	public static void main(String[] args) {
		EntityManager em = null;
		try {
			em = JpaUtil.getEntityManagerFactory().createEntityManager();
			IEventoRepository eventoRepo = new EventoRepository(em);
			IPartitaRepository partitaReport = new PartitaRepository(em);
			IRepository<Persona, Long> personaRepo = new Repository<Persona>(em, Persona.class);
			IRepository<Location, Long> locationRepo = new Repository<Location>(em, Location.class);
			IRepository<Partecipazione, Long> partecipazioneRepo = new Repository<Partecipazione>(em,
					Partecipazione.class);
			IRepository<PartitaDiCalcio, Long> partitaRepo = new Repository<PartitaDiCalcio>(em, PartitaDiCalcio.class);
			IRepository<GaraDiAtletica, Long> garaRepo = new Repository<GaraDiAtletica>(em, GaraDiAtletica.class);
			IRepository<Concerto, Long> concertoRepo = new Repository<Concerto>(em, Concerto.class);

			AbstractEventoService ev = new EventoService(em, eventoRepo, partitaReport, personaRepo, locationRepo,
					partecipazioneRepo, partitaRepo, garaRepo, concertoRepo);

			Location l1 = new Location(0, "Maradona", "Napoli");
			ev.registraLocation(l1);

			Location l2 = new Location(0, "SanSiro", "Milano");
			ev.registraLocation(l2);

			Location l3 = new Location(0, "Olimpico", "Roma");
			ev.registraLocation(l3);

			Location l4 = new Location(0, "SanNicola", "Bari");
			ev.registraLocation(l4);

			Persona p1 = new Persona(0, "Mario", "Rossi", "mario.rossi@gmail.com", LocalDate.of(1989, 6, 5),
					Sesso.MASCHIO, new ArrayList<Partecipazione>());
			ev.registraPersona(p1);
			Persona p2 = new Persona(0, "Gianni", "Rossi", "gianni.rossi@gmail.com", LocalDate.of(1959, 6, 5),
					Sesso.MASCHIO, new ArrayList<Partecipazione>());
			ev.registraPersona(p2);
			Persona p3 = new Persona(0, "Gianni", "Bianchi", "gianni.bianchi@gmail.com", LocalDate.of(1999, 8, 5),
					Sesso.MASCHIO, new ArrayList<Partecipazione>());
			ev.registraPersona(p3);
			Persona p4 = new Persona(0, "Anna", "Bianchi", "anna.bianchi@gmail.com", LocalDate.of(1979, 8, 4),
					Sesso.FEMMINA, new ArrayList<Partecipazione>());
			ev.registraPersona(p4);
			Persona p5 = new Persona(0, "Anna", "Rossi", "anna.rossi@gmail.com", LocalDate.of(1977, 8, 4),
					Sesso.FEMMINA, new ArrayList<Partecipazione>());
			ev.registraPersona(p5);

			PartitaDiCalcio pDC1 = new PartitaDiCalcio(0, "Finalissima", LocalDate.of(2002, 10, 15), "finale di coppa",
					TipoEvento.PUBBLICO, 50000, l1, new HashSet<Partecipazione>(), "Napoli", "Milan", SquadraVincente.TRASFERTA, 0, 1);
			ev.registraPartita(pDC1);

			PartitaDiCalcio pDC2 = new PartitaDiCalcio(0, "Finale", LocalDate.of(2005, 10, 10), "partita scudetto",
					TipoEvento.PUBBLICO, 80000, l2, new HashSet<Partecipazione>(), "Inter", "Juve", SquadraVincente.CASA, 3, 1);
			ev.registraPartita(pDC2);

			PartitaDiCalcio pDC3 = new PartitaDiCalcio(0, "ScontroDiretto", LocalDate.of(2008, 8, 10), "playOff",
					TipoEvento.PUBBLICO, 15000, l3, new HashSet<Partecipazione>(), "Roma", "Lazio", SquadraVincente.CASA, 3, 1);
			ev.registraPartita(pDC3);

			PartitaDiCalcio pDC4 = new PartitaDiCalcio(0, "ScontroDiretto", LocalDate.of(2008, 8, 10), "playOff",
					TipoEvento.PUBBLICO, 60000, l4, new HashSet<Partecipazione>(), "Bari", "Lazio", SquadraVincente.PAREGGIO, 0, 0);
			ev.registraPartita(pDC4);
			
			//  fine entities
			
			List<SquadraVincente> listaRisultati = new ArrayList<>();
			listaRisultati.add(SquadraVincente.CASA);
			List<PartitaDiCalcio> vittoriaDiCasa = ev.getPartiteVinteInCasa(listaRisultati);
			
			for ( PartitaDiCalcio x : vittoriaDiCasa) {
				System.out.println(x.getDescrizione());
			}

		} finally {
			// if(!em.equals(null)) {
			if (em != null) {
				em.close();
			}
		}
	}

}
